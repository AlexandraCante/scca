<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Simple Responsive Timeline</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">


      <link rel="stylesheet" href="../css/timeline.css">

  
</head>

<body>
  <script src="https://use.typekit.net/bkt6ydm.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

    <div class="row example-split">
        <div class="col-md-12 example-title">
            <h2>Programación</h2>
        </div>
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
			<ul class="timeline timeline-split">
		<?php
			include_once('');
			$con = conectar();
			if(!con){
				die("imposible conectarse: ".mysqli_error($con));
			}
			if($mysqli_connect_errno()){
				die("Connect failed: ".mysqli_connect_errno()." : ".mysqli_connect_error());
			}
			$query = mysqli_query($con,"SELECT pr.idProgramacion,month(fechaInicio) as mes,fechaInicio, hora,titulo,tipoCapacitacion,nivel,pr.fechaReg
			FROM programacion as pr left join archivo as ar on pr.idProgramacion=ar.idProgramacion 
			where visibilidad=1 and year(fechaInicio)=year(now())+1 order by mes");

			$mesInicio = 0;

			while($row = mysqli_fetch_array($query, MYSQL_ASSOC))
			{
				if ($mesInicio != $row['mes']){
					$numeroMes  = $row['mes'];
					$mesInicio = $numeroMes;
					$dateObj   = DateTime::createFromFormat('!m', $numeroMes);
					$nombreMes = $dateObj->format('F');
					$htmlScriptMes = '<li class="timeline-item period">
										<div class="timeline-info"></div>
										<div class="timeline-marker"></div>
										<div class="timeline-content">
											<h2 class="timeline-title">'.$nombreMes'</h2>
										</div>
									</li>';
					echo $htmlScriptMes;
				}
				
				$htmlScriptEvento = '<li class="timeline-item">
										<div class="timeline-info">
											<span>'.$row['fechaInicio']. '</span>
										</div>
										<div class="timeline-marker"></div>
										<div class="timeline-content">
											<h3 class="timeline-title">'.$row['titulo'].'</h3>
											<p>'.$row['fechaInicio'].' Nivel: '.$nivel.' Hora : '. $row['hora'].'</p>
										</div>
									</li>';
				echo $htmlScriptEvento;     
			
			}?>
			</ul>
		</div>
    </div>

  
</body>
</html>
